let guitars = {
  jackson: [
      {
          name: 'Jackson Pro CD24',
          id: '0001',
          price: 1360,
          scale_length: 648,
          number_of_frets: 24,
          nut_width: 43,
          color: 'white',
          photo: []

      },
      {
          name: 'Jackson JS32',
          id: '0002',
          price: 683,
          scale_length: 648,
          number_of_frets: 24,
          nut_width: 43,
          color: 'black',
          photo: []

      },
      {
          name: 'Jackson RR1',
          id: '0003',
          price: 3190,
          scale_length: 648,
          number_of_frets: 22,
          nut_width: 43,
          color: 'black',
          photo: []

      },
      {
          name: 'Jackson Soloist',
          id: '0004',
          price: 1360,
          scale_length: 648,
          number_of_frets: 24,
          nut_width: 43,
          color: 'black',
          photo: []

      },
      {
          name: 'Jackson USA Select KE2',
          id: '0005',
          price: 4081,
          scale_length: 648,
          number_of_frets: 24,
          nut_width: 43,
          color: 'white',
          photo: []

      },
      {
          name: 'Jackson WRXMG',
          id: '0006',
          price: 549,
          scale_length: 648,
          number_of_frets: 24,
          nut_width: 43,
          color: 'black',
          photo: []

      },
      {
          name: 'Jackson XKV7',
          id: '0007',
          price: 899,
          scale_length: 648,
          number_of_frets: 24,
          nut_width: 43,
          color: 'black',
          photo: []

      },
      {
          name: 'Jackson rrx24',
          id: '0008',
          price: 1020,
          scale_length: 648,
          number_of_frets: 24,
          nut_width: 43,
          color: 'red',
          photo: []

      },

  ],
  ibanez: [
      {
          name: 'Ibanez GRG7221 QA',
          id: '0009',
          price: 320,
          scale_length: 648,
          number_of_frets: 24,
          nut_width: 43,
          color: 'grey',
          photo: []
      },
      {
          name: 'Ibanez GRGM21QA',
          id: '0010',
          price: 179,
          scale_length: 564,
          number_of_frets: 24,
          nut_width: 41,
          color: 'black',
          photo: []
      },
      {
          name: 'Ibanez GRGM21M',
          id: '0011',
          price: 149,
          scale_length: 564,
          number_of_frets: 24,
          nut_width: 41,
          color: 'blue',
          photo: []
      },
      {
          name: 'Ibanez GRX70QA',
          id: '0012',
          price: 229,
          scale_length: 648,
          number_of_frets: 22,
          nut_width: 43,
          color: 'green',
          photo: []
      },
      {
          name: 'Ibanez GRG170DXBKN',
          id: '0013',
          price: 315,
          scale_length: 648,
          number_of_frets: 22,
          nut_width: 43,
          color: 'black',
          photo: []
      },
  ],
  hamer: [
      {
          name: 'Hamer California',
          id: '0014',
          price: 274,
          scale_length: 648,
          number_of_frets: 24,
          nut_width: 43,
          color: 'orange',
          photo: []
      },
      {
          name: 'Hamer Monaco',
          id: '0015',
          price: 699,
          scale_length: 648,
          number_of_frets: 24,
          nut_width: 43,
          color: 'blue',
          photo: []
      },
      {
          name: 'Hamer Diablo 2',
          id: '0016',
          price: 765,
          scale_length: 648,
          number_of_frets: 24,
          nut_width: 43,
          color: 'orange',
          photo: []
      },
      {
          name: 'Hamer XT',
          id: '0017',
          price: 280,
          scale_length: 648,
          number_of_frets: 24,
          nut_width: 43,
          color: 'orange',
          photo: []
      },
      {
          name: 'Hamer XT Vector',
          id: '0018',
          price: 862,
          scale_length: 648,
          number_of_frets: 24,
          nut_width: 43,
          color: 'orange',
          photo: []
      },
  ],
  gibson: [
      {
          name: 'Gibson Explorer',
          id: '0019',
          price: 1112,
          scale_length: 628,
          number_of_frets: 22,
          nut_width: 43,
          color: 'white',
          photo: []
          
      },
      {
          name: 'Gibson Les Paul',
          id: '0020',
          price: 4460,
          scale_length: 628,
          number_of_frets: 22,
          nut_width: 43,
          color: 'orange',
          photo: []
          
      },
      {
          name: 'Gibson Reverse V',
          id: '0021',
          price: 2516,
          scale_length: 628,
          number_of_frets: 22,
          nut_width: 43,
          color: 'white',
          photo: []
          
      },
      {
          name: 'Gibson SG',
          id: '0022',
          price: 1620,
          scale_length: 629,
          number_of_frets: 22,
          nut_width: 43,
          color: 'red',
          photo: []
          
      },
  ],

  ESP: [
      {
          name: 'ESP EX50',
          id: '0023',
          price: 1620,
          scale_length: 628,
          number_of_frets: 22,
          nut_width: 43,
          color: 'black',
          photo: [] 
      },
      {
          name: 'ESP Hetfield',
          id: '0024',
          price: 1960,
          scale_length: 628,
          number_of_frets: 22,
          nut_width: 42,
          color: 'black',
          photo: [] 
      },
      {
          name: 'ESP EX401FM',
          id: '0025',
          price: 1270,
          scale_length: 628,
          number_of_frets: 22,
          nut_width: 42,
          color: 'black',
          photo: [] 
      },
      {
          name: 'ESP MH1000HS',
          id: '0026',
          price: 1049,
          scale_length: 648,
          number_of_frets: 22,
          nut_width: 42,
          color: 'orange',
          photo: [] 
      },
      {
          name: 'ESP Truckster',
          id: '0027',
          price: 2526,
          scale_length: 648,
          number_of_frets: 22,
          nut_width: 42,
          color: 'grey',
          photo: [] 
      },
  ]

}