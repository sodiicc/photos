import React, { useState, useEffect } from "react";
import Slider from "react-animated-slider";
import "react-animated-slider/build/horizontal.css";
import styled from "styled-components";
import { MostPopular } from "./MostPopular";
import SliderItem from "./SliderItem";
import "../../App.css";

export const HomePage = props => {
  const [photo, setPhoto] = useState([])
  let photosArr = ['0006', '0004', '0012', '0020']

  const sliderInfo = async() => { 
    let bbb=[]
    for (let i  =0; i< photosArr.length; i++){
      const response = await fetch(`/products/${photosArr[i]}`);
      const responseJSON = await response.json();
      bbb.push(responseJSON.photo[0])
    }
      setPhoto(bbb)

};

useEffect(()=>{
  sliderInfo()
}, [])

  return (
    <div>

    <SliderContainer>
      {
      console.log('photo', photo )

      }
      <Slider
      autoplay={800}
       >
        <div
          >
          <SliderItem 
          name="Fender"
          price="1859"
          url={`/static/img/${photo[0]}`}
          />
        </div>
        <div
          >
          <SliderItem 
          name="Gibson"
          price="4150"
          url={`/static/img/${photo[1]}`}
          />
        </div>
        <div
          >
          <SliderItem 
          name="Hamer"
          price="890"
          url={`/static/img/${photo[2]}`}
          />
        </div>
        <div
          >
          <SliderItem 
          name="Jackson"
          price="2470"
          url={`/static/img/${photo[3]}`}
          />
        </div>
        
      </Slider>
    </SliderContainer>
      <MostPopular
        title="Gibson"
        url="https://medias.audiofanzine.com/images/normal/schecter-reaper-7-multiscale-2402191.png"
      />
      <MostPopular
        title="Gibson"
        url="https://medias.audiofanzine.com/images/normal/schecter-nick-johnston-traditional-2392118.png"
      />
      <MostPopular
        title="Gibson"
        url="https://www.schecterguitars.com/images/store/product/CHRIS%20HOWORTH%202018%20GRAPHIC%20TILT.png"
      />
      <MostPopular
        title="Gibson"
        url="https://www.deanguitars.com/images/productimages/vsthbks/vsthbks.png"
      />
      <MostPopular
        title="Gibson"
        url="https://dk1xgl0d43mu1.cloudfront.net/user_files/esp/product_images/000/009/842/xlarge.png?1389979422"
      />
      <MostPopular
        title="Gibson"
        url="https://www.schecterguitars.com/images/store/product/V-1%20PLATINUM%20TILT.png"
      />
</div>
  );
};

const SliderContainer = styled.div`
background: url(http://www.vichealth.vic.gov.au/-/media/images/vichealth/images-and-files/funding/female-participation-in-sport/cogfunding1-1200x675.jpg) no-repeat center center;
  text-align: center;
  color: #f07;
  @keyframes shadow {
from {color: white;
  background-color: #f07;}
to {color: black;
  background-color: #70f;}
}
  span {
    padding: 8px;
    color: #f07;
    font-size: 28px;
    border: solid 4px #f07;
    border-radius: 5px;
  }
  img {
  height: 450px;
  object-fit: contain;
}
  button {
   animation: shadow .8s infinite linear;
   font-size: 22px;
    background-color: #f07;
    border: none;
    padding: 10px 20px;
    border-radius: 5px;
    color: white;
  }
  .slider {
    height: 600px;
  }

  .most-popular {
    width: 450px;
    height: 200px;
    object-fit: contain;
  }

  .most-popular-wrapper {
    display: inline-block;
    margin: 20px;
    color: #f07;
    font-size: 22px;
    background-color: #159;
    border-radius: 15px;
  }
`;
